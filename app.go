package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	firebase "firebase.google.com/go"
	"google.golang.org/api/option"
)

const (
	limit     = "3"
	redditUrl = "https://www.reddit.com/r/all/top/.json?limit=" + limit
	// hackerNewsUrl = "TODO"
	firebaseDbUrl = "https://slacc1.firebaseio.com"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func getTodaysPosts() []map[string]interface{} {
	reply, err := http.Get(redditUrl)
	check(err)
	defer reply.Body.Close()

	body, err := io.ReadAll(reply.Body)
	check(err)

	var snapshot map[string]interface{}
	err = json.Unmarshal([]byte(body), &snapshot)
	check(err)

	type RedditResponse struct {
		Data struct {
			Children []map[string]interface{} `json:"children"`
		} `json:"data"`
	}

	var resp RedditResponse
	if err := json.Unmarshal([]byte(body), &resp); err != nil {
		panic(err)
	}

	children := resp.Data.Children

	var posts []map[string]interface{}
	for _, post := range children {
		// Debugging
		fmt.Println(post)

		postData, exists := post["data"].(map[string]interface{})
		if !exists {
			panic(fmt.Errorf("Failed to parse data field on a row"))
		}

		delete(postData, "media_metadata")

		posts = append(posts, postData)
	}
	// Debugging
	fmt.Println("Posts: ", posts)

	return posts
}

// today := time.Now().UTC().Format("2006-01-02")
// queryKey := "reddit-" + today
func main() {
	ctx := context.Background()

	app, err := firebase.NewApp(
		ctx,
		&firebase.Config{ProjectID: "slacc1"},
		option.WithAPIKey("AIzaSyDXPxvO2bTyonaj0T-iTabnFg_b6wnA9sg"),
	)
	if err != nil {
		check(err)
	}

	db, err := app.Firestore(ctx)
	check(err)
	defer db.Close()

	Posts := db.Collection("go-posts")

	// check if todays thing exists in firebase, if it does not, then fetch from http.Get
	// docs, err := Posts.OrderBy("created", firestore.Desc).Limit(limit).Documents(ctx).GetAll()
	// check(err)
	// doc := docs[0]
	// if doc.CreateTime.Before(time.Now().Add(-24 * time.Hour)) {
	// }

	posts := getTodaysPosts()
	for _, post := range posts {
		_, _, err := Posts.Add(ctx, post)
		check(err)
	}
}
