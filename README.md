## Go Posts/Articles

Simple Go app to retrieve Reddit (and maybe in the future Hacker News) top 10/100 posts and store
them somewhere. Thinking Supabase Postgres or Firebase.

It's kind of silly to build a cache mechanism using a local json file
to avoid 429 errors from the Reddit API. The data is going into Firebase most likely
to be as lazy as possible with which fields we want to retain. The cache mechanism
will likely be used again in the near future, plus it's good to get practice building
systems of various size in Go.

##### Bonus

- fetch and save comments from post
